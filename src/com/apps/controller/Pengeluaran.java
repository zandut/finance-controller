package com.apps.controller;

/**
 * 
 * Class Pengeluaran
 *
 */
public class Pengeluaran {
	private int id_pengeluaran;
	private String nama_pengeluaran;
	private String tanggal;
	private String jam;
	private long jumlah;
	
	/**
	 * untuk mendapatkan id Pengeluaran
	 * @return the id_pengeluaran
	 */
	public int getId_pengeluaran() {
		return id_pengeluaran;
	}
	/**
	 * untuk mengeset Id Pengeluaran
	 * @param id_pengeluaran the id_pengeluaran to set
	 */
	public void setId_pengeluaran(int id_pengeluaran) {
		this.id_pengeluaran = id_pengeluaran;
	}
	/**
	 * untuk mendapatkan nama/judul pengeluaran
	 * @return the nama_pengeluaran
	 */
	public String getNama_pengeluaran() {
		return nama_pengeluaran;
	}
	/**
	 * untuk mengeset nama/judul pengeluaran
	 * @param nama_pengeluaran the nama_pengeluaran to set
	 */
	public void setNama_pengeluaran(String nama_pengeluaran) {
		this.nama_pengeluaran = nama_pengeluaran;
	}
	/**
	 * untuk mendapatkan tanggal
	 * @return the tanggal
	 */
	public String getTanggal() {
		return tanggal;
	}
	/**
	 * untuk mengeset tanggal
	 * @param tanggal the tanggal to set
	 */
	public void setTanggal(String tanggal) {
		this.tanggal = tanggal;
	}
	/**
	 * untuk mendapatkan jam
	 * @return the jam
	 */
	public String getJam() {
		return jam;
	}
	/**
	 * untuk mengeset jam
	 * @param jam the jam to set
	 */
	public void setJam(String jam) {
		this.jam = jam;
	}
	/**
	 * untuk mendapatkan jumlah/harga
	 * @return the jumlah
	 */
	public long getJumlah() {
		return jumlah;
	}
	/**
	 * untuk mengeset jumlah/harga
	 * @param jumlah the jumlah to set
	 */
	public void setJumlah(long jumlah) {
		this.jumlah = jumlah;
	}
	
	
	

}
