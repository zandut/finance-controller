package com.apps.controller;

/**
 * 
 * Class Pemasukan
 *
 */
public class Pemasukan {
	private int id_pemasukan;
	private String nama_pemasukan;
	private String tanggal;
	private String jam;
	private long jumlah;
	
	/**
	 * Untuk mendapatkan Id Pemasukan
	 * @return the id_pemasukan
	 */
	public int getId_pemasukan() {
		return id_pemasukan;
	}
	/**
	 * Untuk mengeset Id Masukkan
	 * @param id_pemasukan the id_pemasukan to set
	 */
	public void setId_pemasukan(int id_pemasukan) {
		this.id_pemasukan = id_pemasukan;
	}
	/**
	 * Untuk Mendapatkan nama/judul pemasukan
	 * @return the nama_pemasukan
	 */
	public String getNama_pemasukan() {
		return nama_pemasukan;
	}
	/**
	 * Untuk mengeset nama/judul pemasukan
	 * @param nama_pemasukan the nama_pemasukan to set
	 */
	public void setNama_pemasukan(String nama_pemasukan) {
		this.nama_pemasukan = nama_pemasukan;
	}
	/**
	 * untuk mendapatkan tanggal
	 * @return the tanggal
	 */
	public String getTanggal() {
		return tanggal;
	}
	/**
	 * untuk mengeset tanggal
	 * @param tanggal the tanggal to set
	 */
	public void setTanggal(String tanggal) {
		this.tanggal = tanggal;
	}
	/**
	 * untuk mendapatkan jam
	 * @return the jam
	 */
	public String getJam() {
		return jam;
	}
	/**
	 * untuk mengeset jam
	 * @param jam the jam to set
	 */
	public void setJam(String jam) {
		this.jam = jam;
	}
	/**
	 * untuk mendapatkan jumlah/harga
	 * @return the jumlah
	 */
	public long getJumlah() {
		return jumlah;
	}
	/**
	 * untuk mengeset jumlah.harga
	 * @param jumlah the jumlah to set
	 */
	public void setJumlah(long jumlah) {
		this.jumlah = jumlah;
	}
	
	
	

}
