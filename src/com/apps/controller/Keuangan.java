package com.apps.controller;

/**
 * 
 * Class Keuangan
 *
 */
public class Keuangan {
	private int id_keuangan;
	private long saldo;
	
	/**
	 * Untuk mendapatkan Id Keuangan
	 * @return the id_keuangan
	 */
	public int getId_keuangan() {
		return id_keuangan;
	}
	/**
	 * Untuk Mengeset Id Keuangan
	 * @param id_keuangan the id_keuangan to set
	 */
	public void setId_keuangan(int id_keuangan) {
		this.id_keuangan = id_keuangan;
	}
	/**
	 * Untuk mendapatkan Saldo
	 * @return the saldo
	 */
	public long getSaldo() {
		return saldo;
	}
	/**
	 * Untuk mengeset Saldo
	 * @param saldo the saldo to set
	 */
	public void setSaldo(long saldo) {
		this.saldo = saldo;
	}
	
	
	

}
