package com.apps.view;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.apps.R;
import com.apps.controller.Pengeluaran;
import com.apps.models.Database;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * 
 * Class activity input Pengeluaran
 *
 */
public class Activity_input_pengeluaran extends Activity{
	
	private EditText nama_pengeluaran;
	private EditText jumlah;
	private Button simpan;
	
	/**
	 * Method yang dijalankan ketika activity ini dijalankan
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_input_pengeluaran);
		
		final Database db = new Database(getApplicationContext());
		
		 try {
	        	
				db.createDatabase();
				db.openDatabase();
				
			} catch (Exception e) {
				Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
			}
		 
		nama_pengeluaran = (EditText) findViewById(R.id.editText1);
		jumlah = (EditText) findViewById(R.id.editText2);
		simpan = (Button) findViewById(R.id.button1);
		
		/**
		 * Ketika button simpan di tekan
		 */
		simpan.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				Date date = new Date();
				SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
				SimpleDateFormat time = new SimpleDateFormat("HH:mm");
				String tgl = format.format(date);
				String jam = time.format(date);
				
				Pengeluaran p = new Pengeluaran();
				p.setNama_pengeluaran(nama_pengeluaran.getText().toString());
				p.setJumlah(Long.parseLong(jumlah.getText().toString()));
				p.setJam(jam);
				p.setTanggal(tgl);
				
				if (db.insertPengeluaran(p))
				{
					Toast.makeText(getApplicationContext(), "Berhasil !", Toast.LENGTH_SHORT).show();
					onBackPressed();
				}
			}
		});
	}

	/**
	 * Ketika tombol back di tekan
	 */
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		this.finish();
		startActivity(new Intent(getApplicationContext(), Activity_main.class));
	}
}
