package com.apps.view;

import com.apps.R;
import com.apps.controller.Pemasukan;
import com.apps.models.Database;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 
 * Class activity Edit Pemasukan
 *
 */
public class Activity_edit_pemasukan extends Activity{

	private TextView textID;
	private TextView textTanggal;
	
	private EditText editNamaPemasukan;
	private EditText editJumlah;
	
	private Database db;
	
	
	/**
	 * Method yang dijalankan ketika activity ini dijalankan
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_edit_pemasukan);
		
		textID = (TextView) findViewById(R.id.textView1);
		textTanggal = (TextView) findViewById(R.id.textView2);
		
		editNamaPemasukan = (EditText) findViewById(R.id.editText1);
		editJumlah = (EditText) findViewById(R.id.editText2);
		
		Button btnSimpan = (Button) findViewById(R.id.button1);
		
		
		
		db = new Database(getApplicationContext());
		
		try {
			db.createDatabase();
			db.openDatabase();
		} catch (Exception e) {
			// TODO: handle exception
			Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
		}
		
		final int id_pemasukan = Integer.parseInt(getIntent().getExtras().getString("id_pemasukan"));
		
		Pemasukan p = db.getPemasukanById(id_pemasukan);
		
		textID.setText("ID Pemasukan : "+p.getId_pemasukan());
		textTanggal.setText("Tanggal Jam : "+p.getTanggal()+" "+p.getJam());
		
		editNamaPemasukan.setText(p.getNama_pemasukan());
		editJumlah.setText(""+p.getJumlah());
		
		/**
		 * Ketika button simpan di tekan
		 */
		btnSimpan.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (db.updatePemasukan(id_pemasukan, editNamaPemasukan.getText().toString(), editJumlah.getText().toString()))
				{
					Toast.makeText(Activity_edit_pemasukan.this, "Berhasil !!", Toast.LENGTH_SHORT).show();
					onBackPressed();
				}
				
			}
		});

	
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		return super.onCreateOptionsMenu(menu);
	}
	
	/**
	 * Ketika tombol back di tekan
	 */
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		this.finish();
		startActivity(new Intent(getApplicationContext(), Activity_Pemasukan.class));
	}
}
