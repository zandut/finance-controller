package com.apps.view;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.apps.R;
import com.apps.controller.Pemasukan;
import com.apps.models.Database;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ApplicationErrorReport.AnrInfo;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import android.widget.AdapterView.OnItemLongClickListener;

import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 
 * Class Pemasukan
 *
 */
public class Activity_Pemasukan extends Activity{
	private ListView list1;
	private TextView text1;
	private Database db = null;
	private String bulan = new String();
	
	/**
	 * Method yang akan dijalankan ketika activity di jalankan
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_pemasukan);
		
		db = new Database(getApplicationContext());
		try {
			db.createDatabase();
			db.openDatabase();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			
		}
		
		list1 = (ListView) findViewById(R.id.listView1);
		text1 = (TextView) findViewById(R.id.TextView01);
		
		
		SimpleDateFormat format = new SimpleDateFormat("MM");
		bulan = format.format(new Date());
		ArrayList<Pemasukan> daftar1 = db.getPemasukan(bulan);
		long jml = 0;
		for (Pemasukan p : daftar1)
		{
			jml += p.getJumlah();
		}
		
		text1.setText(""+jml);

		list1.setAdapter(new ArrayAdapterPemasukan(getApplicationContext(), daftar1));
		
		/**
		 * Ketika item list di klik
		 */
		list1.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				
				Intent intent = new Intent(getBaseContext(), Activity_edit_pemasukan.class);
				Pemasukan p = (Pemasukan) list1.getItemAtPosition(position);
				intent.putExtra("id_pemasukan", ""+p.getId_pemasukan());
				startActivity(intent);
			}
		
		});
		
		/**
		 * Ketika item list di longClick
		 */
		list1.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				// TODO Auto-generated method stub
				final Pemasukan p = (Pemasukan) list1.getItemAtPosition(position);
				
				new AlertDialog.Builder(Activity_Pemasukan.this)
				.setIcon(android.R.drawable.ic_dialog_alert)
				.setTitle("Konfirmasi")
				.setMessage("Yakin menghapus ?")
				.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						// TODO Auto-generated method stub
						if (db.deletePemasukan(p.getId_pemasukan()))
						{
							Toast.makeText(Activity_Pemasukan.this, "Berhasil !!", Toast.LENGTH_SHORT).show();
							
							ArrayList<Pemasukan> daftar1 = db.getPemasukan(bulan);
							long jml = 0;
							for (Pemasukan p : daftar1)
							{
								jml += p.getJumlah();
							}
							
							text1.setText(""+jml);
							list1.setAdapter(new ArrayAdapterPemasukan(getApplicationContext(), daftar1));
						}
						
					}
				})
				.setNegativeButton("Tidak", null)
				.show();
				
				return false;
			}
			
			
			

		
		});
		
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		getMenuInflater().inflate(R.menu.menu_pendapatan, menu);
		return true;
	}
	
	/**
	 * Ketika tombol menu di klik
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case R.id.item1:
			startActivity(new Intent(getApplicationContext(), Activity_input_pemasukan.class));
			this.finish();
			break;

		case R.id.item2:
			new AlertDialog.Builder(this)
			.setIcon(android.R.drawable.ic_dialog_alert)
			.setTitle("Konfirmasi")
			.setMessage("Yakin menghapus SEMUA pemasukan ?")
			.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					// TODO Auto-generated method stub
					if (db.deletePemasukan())
					{
						Toast.makeText(Activity_Pemasukan.this, "Berhasil !!", Toast.LENGTH_SHORT).show();
						
						ArrayList<Pemasukan> daftar1 = db.getPemasukan(bulan);
						long jml = 0;
						for (Pemasukan p : daftar1)
						{
							jml += p.getJumlah();
						}
						
						text1.setText(""+jml);
						list1.setAdapter(new ArrayAdapterPemasukan(getApplicationContext(), daftar1));
					}
					
				}
			})
			.setNegativeButton("Tidak", null)
			.show();
			
			break;
			
		case R.id.item3:	
			startActivity(new Intent(getApplicationContext(), Activity_main.class));
			this.finish();
			break;
		}
		return true;
	}
	
	/**
	 * Ketika tombol back di klik
	 */
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		this.finish();
		startActivity(new Intent(getBaseContext(), Activity_main.class));
	}

}
