package com.apps.view;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.apps.R;
import com.apps.controller.Pemasukan;
import com.apps.models.Database;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * 
 * Class activity Input Pemasukan
 *
 */
public class Activity_input_pemasukan extends Activity{
	
	private EditText edit1;
	private EditText edit2;
	private Button button1;
	
	/**
	 * Method yang dijalankan ketika activity ini dijalankan
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_input_pemasukan);
		
		final Database db = new Database(getApplicationContext());
		
		try {
			db.createDatabase();
			db.openDatabase();
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		edit1 = (EditText) findViewById(R.id.editText1);
		edit2 = (EditText) findViewById(R.id.editText2);
		button1 = (Button) findViewById(R.id.button1);
		
		/**
		 * Ketika button1 di tekan
		 */
		button1.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Date date = new Date();
				SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
				SimpleDateFormat time = new SimpleDateFormat("HH:mm");
				String tgl = format.format(date);
				
				String jam = time.format(date);
				
				Pemasukan pemasukan = new Pemasukan();
				pemasukan.setNama_pemasukan(edit1.getText().toString());
				pemasukan.setTanggal(tgl);
				pemasukan.setJam(jam);
				pemasukan.setJumlah(Long.parseLong(edit2.getText().toString()));
				
				if (db.insertPemasukan(pemasukan))
				{
					Toast.makeText(getApplicationContext(), "Berhasil !", Toast.LENGTH_SHORT).show();
					onBackPressed();
				}
			}
		});
		
	}
	
	/**
	 * Ketika tombol back di tekan
	 */
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		this.finish();
		startActivity(new Intent(getApplicationContext(), Activity_Pemasukan.class));
		
	}

}
