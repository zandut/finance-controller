package com.apps.view;

import com.apps.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * 
 * Class activity Login
 * 
 */
public class ActivityLogin extends Activity {

	private EditText txtUsername;
	private EditText txtPassword;
	private Button btnLogin;
	private final String username = "admin";
	private final String password = "akugantengsangat";

	/**
	 * Method yang akan dijalakan ketika activity ini di jalakan
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);

		txtPassword = (EditText) findViewById(R.id.editText2);
		txtUsername = (EditText) findViewById(R.id.editText1);

		btnLogin = (Button) findViewById(R.id.button1);

		/**
		 * Ketika button login di tekan
		 */
		btnLogin.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				String msg = "";
				if (txtUsername.getText().toString().equals(username)
						&& txtPassword.getText().toString().equals(password)) {
					
					msg = "Sukses !!!";
					startActivity(new Intent(ActivityLogin.this, Activity_main.class));
					finish();
					
				}
				else
				{
					msg = "Login Gagal !!!";
				}
				
				Toast.makeText(ActivityLogin.this, msg, Toast.LENGTH_SHORT).show();

			}
		});

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		System.exit(0);
	}

}
