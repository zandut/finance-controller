package com.apps.view;

import java.util.ArrayList;

import com.apps.R;
import com.apps.controller.Pengeluaran;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * 
 * Class Adapter dari Pengeluaran
 *
 */
public class ArrayAdapterPengeluaran extends ArrayAdapter<Pengeluaran>{

	private Context myContext;
	private ArrayList<Pengeluaran> daftar = new ArrayList<Pengeluaran>();
	/**
	 * Constructor
	 * @param context
	 * @param objects
	 */
	public ArrayAdapterPengeluaran(Context context, ArrayList<Pengeluaran> objects) {
		
		
		super(context, R.layout.layout_list, objects);
		// TODO Auto-generated constructor stub
		this.myContext = context;
		this.daftar = objects;
	}
	
	/**
	 * Untuk mengambil component dari xml /layout/layout_list
	 * dan mengubah berdasarkan attibute dari class Pengeluaran
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		LayoutInflater inflater = (LayoutInflater) myContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View row = (View) inflater.inflate(R.layout.layout_list, parent, false);
		
		TextView text1 = (TextView) row.findViewById(R.id.textView1);
		TextView text2 = (TextView) row.findViewById(R.id.textView2);
		TextView text3 = (TextView) row.findViewById(R.id.textView3);
		
		text1.setText(daftar.get(position).getTanggal()+" "+daftar.get(position).getJam());
		text2.setText(daftar.get(position).getNama_pengeluaran());
		text3.setText("Rp. "+daftar.get(position).getJumlah());
		
		return row;
	}
	
	
}
