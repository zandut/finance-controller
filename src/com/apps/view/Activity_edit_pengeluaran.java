package com.apps.view;

import com.apps.R;
import com.apps.controller.Pengeluaran;
import com.apps.models.Database;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 
 * Class activity Edit Pengeluaran
 *
 */
public class Activity_edit_pengeluaran extends Activity{
	private TextView textIDPengeluaran;
    private TextView textTanggal;
    private EditText editNamaPengeluaran;
    private EditText editJumlah;
    private String id = new String();
    private Database db = null;

    /**
     * Method yang dijalankan ketika activity ini dijalankan
     */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_edit_pengeluaran);
		
		textIDPengeluaran = (TextView) findViewById(R.id.textView1);
		textTanggal = (TextView) findViewById(R.id.textView2);
		
		editNamaPengeluaran = (EditText) findViewById(R.id.editText1);
		editJumlah = (EditText) findViewById(R.id.editText2);
		
		Button btnSimpan = (Button) findViewById(R.id.button1);
		
		db = new Database(this);
		try {
			db.createDatabase();
			db.openDatabase();
		} catch (Exception ex) {
			// TODO: handle exception
			Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
		}
		
		Intent intent = getIntent();
		id = intent.getExtras().getString("id_pengeluaran");
		
		Pengeluaran p = db.getPengeluaran(Integer.parseInt(id));
		
		textIDPengeluaran.setText("ID Pengeluaran : "+p.getId_pengeluaran());
		textTanggal.setText("Tanggal Jam : "+p.getTanggal()+" "+p.getJam());
		
		editNamaPengeluaran.setText(p.getNama_pengeluaran());
		editJumlah.setText(""+p.getJumlah());
		
		/**
		 * Ketika button simpan di tekan
		 */
		
		btnSimpan.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (db.updatePengeluaran(Integer.parseInt(id), editNamaPengeluaran.getText().toString(), editJumlah.getText().toString()))
				{
					Toast.makeText(Activity_edit_pengeluaran.this, "Berhasil !!", Toast.LENGTH_SHORT).show();
					onBackPressed();
				}
			}
		});
	
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		return super.onCreateOptionsMenu(menu);
	}
	
	/**
	 * Ketika tombol back di tekan
	 */
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
//		super.onBackPressed();
		this.finish();
		startActivity(new Intent(this, Activity_main.class));
	}
}
