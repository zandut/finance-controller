package com.apps.view;

import java.util.ArrayList;

import com.apps.R;
import com.apps.controller.Pemasukan;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * 
 * Class Adapter untuk Pemasukan
 *
 */
public class ArrayAdapterPemasukan extends ArrayAdapter<Pemasukan>{
	
	private Context myContext;
	private ArrayList<Pemasukan> daftar = new ArrayList<Pemasukan>();
	
	/**
	 * Constructor
	 * @param context
	 * @param daftar
	 */
	public ArrayAdapterPemasukan(Context context, ArrayList<Pemasukan> daftar) {
		// TODO Auto-generated constructor stub
		super(context, R.layout.layout_list, daftar);
		this.myContext = context;
		this.daftar = daftar;
	}
	
	/**
	 * Untuk mengambil component dari xml /layout/layout_list 
	 * dan mengubah berdasarkan attribute dari class Pemasukan
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		LayoutInflater layout = (LayoutInflater) myContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View row = (View) layout.inflate(R.layout.layout_list, parent, false);
		
		TextView text1 = (TextView) row.findViewById(R.id.textView1);
		TextView text2 = (TextView) row.findViewById(R.id.textView2);
		TextView text3 = (TextView) row.findViewById(R.id.textView3);
		
		text1.setText(daftar.get(position).getTanggal()+" "+daftar.get(position).getJam());
		text2.setText(daftar.get(position).getNama_pemasukan());
		text3.setText("Rp. "+daftar.get(position).getJumlah());
		return row;
	}

}
