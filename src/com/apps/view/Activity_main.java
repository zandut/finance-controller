package com.apps.view;

import java.io.IOException;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;

import com.apps.R;
import com.apps.controller.Keuangan;
import com.apps.controller.Pengeluaran;
import com.apps.models.Database;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.KeyListener;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;

import android.widget.EditText;

import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Class Main
 * 
 */
public class Activity_main extends Activity {
	
	private TextView text2;
	private ListView list1;
	private TextView text01;
	private EditText cariNama;
	private static Database db = null;
	private SimpleDateFormat format = new SimpleDateFormat("MM");



	/**
	 * Method yang akan dijalankan ketika activity ini di jalankan
	 */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_main);
        
        db = new Database(getApplicationContext());
        
        try {
        	
			db.createDatabase();
			db.openDatabase();
			
		} catch (Exception e) {
			Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
		}
        
        cariNama = (EditText) findViewById(R.id.editText1);
        
        /**
         * Ketika cariNama di ketik. 
         */
        cariNama.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
				// TODO Auto-generated method stub
				
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub
				ArrayList<Pengeluaran> daftar = db.getPengeluaran(arg0.toString());
				
				list1.setAdapter(new ArrayAdapterPengeluaran(Activity_main.this, daftar));
				
				
			}
		});
        
        
        text2 = (TextView) findViewById(R.id.textView2);
        list1 = (ListView) findViewById(R.id.listView1);
        text01 = (TextView) findViewById(R.id.TextView01);
        long jml = 0;
        
        ArrayList<Pengeluaran> daftar = db.getAllPengeluaran(format.format(new Date()));
        for (Pengeluaran p : daftar)
        {
        	jml += p.getJumlah();
        }
        
        text01.setText(""+jml);
        
        
        Keuangan uang = db.getKeuangan();
        text2.setText(""+uang.getSaldo());
        
        list1.setAdapter(new ArrayAdapterPengeluaran(getApplicationContext(), daftar));
        
        /**
         * Ketika item di klik
         */
        list1.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				Pengeluaran p = (Pengeluaran) list1.getItemAtPosition(position);
				Intent intent = new Intent(Activity_main.this, Activity_edit_pengeluaran.class);
				intent.putExtra("id_pengeluaran", ""+p.getId_pengeluaran());
				
				startActivity(intent);
				Activity_main.this.finish();
			}
        	
        	
        });
        
        /**
         * Ketika item list di longClick
         */
        list1.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1, final int position, long arg3) {
				// TODO Auto-generated method stub
				final Pengeluaran p = (Pengeluaran) list1.getItemAtPosition(position);
				
				new AlertDialog.Builder(Activity_main.this)
				.setIcon(android.R.drawable.ic_dialog_alert)
				.setTitle("Konfirmasi")
				.setMessage("Yakin menghapus ??")
				.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						// TODO Auto-generated method stub
						if (db.deletePengeluaran(p.getId_pengeluaran()))
						{
							Toast.makeText(Activity_main.this, "Berhasil !!", Toast.LENGTH_SHORT).show();
							long jml = 0;
					        
					        ArrayList<Pengeluaran> daftar = db.getAllPengeluaran(format.format(new Date()));
					        for (Pengeluaran p : daftar)
					        {
					        	jml += p.getJumlah();
					        }
					        
					        text01.setText(""+jml);
					        
					        
					        Keuangan uang = db.getKeuangan();
					        text2.setText(""+uang.getSaldo());
					        
					        list1.setAdapter(new ArrayAdapterPengeluaran(getApplicationContext(), daftar));
							
						}
						
					}
				})
				.setNegativeButton("Tidak", null)
				.show();
				return false;

			
			}
        });
    }

    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    
    
    /**
     * Ketika item menu di pilih
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	// TODO Auto-generated method stub
    	switch (item.getItemId()) {
		case R.id.menu1:
			startActivity(new Intent(getApplicationContext(), Activity_Pemasukan.class));
			this.finish();
			break;

		case R.id.menu2:
			startActivity(new Intent(getApplicationContext(), Activity_input_pengeluaran.class));
			this.finish();
			break;
		case R.id.menu3:
			new AlertDialog.Builder(this)
			.setIcon(android.R.drawable.ic_dialog_alert)
			.setTitle("Konfirmasi")
			.setMessage("Yakin menghapus SEMUA pengeluaran ?")
			.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					// TODO Auto-generated method stub

					if (db.deletePengeluaran())
					{
						Toast.makeText(Activity_main.this, "Berhasil !!", Toast.LENGTH_SHORT).show();
						long jml = 0;
				        
				        ArrayList<Pengeluaran> daftar = db.getAllPengeluaran(format.format(new Date()));
				        for (Pengeluaran p : daftar)
				        {
				        	jml += p.getJumlah();
				        }
				        
				        text01.setText(""+jml);
				        
				        
				        Keuangan uang = db.getKeuangan();
				        text2.setText(""+uang.getSaldo());
				        
				        list1.setAdapter(new ArrayAdapterPengeluaran(getApplicationContext(), daftar));
					}
					
					

					
				}
			})
			.setNegativeButton("Tidak", null)
			.show();
			break;
		}
    	return true;
    }
    
    
    /**
     * Ketika tombol back di tekan
     */
    @Override
    public void onBackPressed() {
    	// TODO Auto-generated method stub
    	new AlertDialog.Builder(this)
		.setIcon(android.R.drawable.ic_dialog_alert)
		.setTitle("Konfirmasi")
		.setMessage("Yakin untuk Logout ?")
		.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(Activity_main.this, ActivityLogin.class);
				startActivity(intent);
				finish();
				
			}
		})
		.setNegativeButton("Tidak, saya tidak ingin. ", null)
		.show();
    	
    }
    
}
