
package com.apps.models;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.apps.controller.Keuangan;
import com.apps.controller.Pemasukan;
import com.apps.controller.Pengeluaran;

import android.R.bool;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

/**
 * 
 * Class Database
 *
 */

public class Database extends SQLiteOpenHelper{
	private static String path;
	private static String db_name = "db_pengeluaran.db";
	private Context myContext;
	private static String tabel_pengeluaran = "pengeluaran";
	private static String tabel_pemasukan = "pemasukan";
	private static String tabel_keuangan = "keuangan";
	
	private SQLiteDatabase myDatabase;
	
	/**
	 * Construtor dari database
	 * @param context
	 */
	public Database(Context context)
	{
		super(context, db_name, null, 1);
		this.myContext = context;
		path = "/data/data/"+myContext.getPackageName()+"/databases/";
		
	}
	
	/**
	 * Untuk membaca database
	 * @throws IOException
	 */
	public void createDatabase() throws IOException
	{
		boolean is_ada = checkDatabase();
		if (is_ada)
		{
			//Toast.makeText(myContext, "Database is exist", Toast.LENGTH_SHORT).show();
		}
		else
		{
			myDatabase = this.getReadableDatabase();
			try
			{
				copyDatabase();
			}
			catch (IOException ex)
			{
				ex.printStackTrace();
				Toast.makeText(myContext, ex.toString(), Toast.LENGTH_LONG).show();
			}
			
		}
	}
	
	
	/**
	 * Untuk mengecek apakah database sudah ada atau belum di data local hp
	 * @return
	 */
	private boolean checkDatabase()
	{
		SQLiteDatabase db = null;
		try
		{
			 String myPath = path + db_name;
			 db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
		}
		catch (SQLiteException ex)
		{
			//Toast.makeText(myContext, "Error check : "+ex.getMessage(), Toast.LENGTH_SHORT).show();
			
		}
		
		if (db != null)
		{
			db.close();
		}
		
		return db != null ? true : false;
	}
	
	/**
	 * Untuk mencopy database ke data local
	 * @throws IOException
	 */
	private void copyDatabase() throws IOException {
		InputStream myInput = myContext.getAssets().open(db_name);
		String outFile = path + db_name;
		OutputStream myOutput = new FileOutputStream(outFile);
		byte[] buffer = new byte[1024];
		int length;
		while ((length = myInput.read(buffer)) > 0)
		{
			myOutput.write(buffer, 0, length);
		}
		
		myOutput.flush();
		myOutput.close();
		myInput.close();
		
		
	}

	@Override
	public void onCreate(SQLiteDatabase arg0) {
		
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
		
			
	}
	
	/**
	 * Untuk connect ke database
	 * @throws SQLiteException
	 */
	public void openDatabase() throws SQLiteException{
		String mypath = path + db_name;
		myDatabase = SQLiteDatabase.openDatabase(mypath, null, SQLiteDatabase.OPEN_READONLY);
	}
	
	/**
	 * Untuk menutup koneksi
	 */
	@Override
	public synchronized void close() {
		
		if (myDatabase != null)
		{
			myDatabase.close();
		}
		
		super.close();
	}
	
	//TODO pengeluaran
	/**
	 * Untuk insert pengeluaran
	 * @param k
	 * @return
	 */
	public boolean insertPengeluaran(Pengeluaran k) {
		SQLiteDatabase db = this.getWritableDatabase();
		boolean masuk = false;
		try
		{
			db.execSQL("insert into "+tabel_pengeluaran+" (nama_pengeluaran, tanggal, jam, jumlah) values('"+k.getNama_pengeluaran()+"','"+k.getTanggal()+"','"+k.getJam()+"','"+k.getJumlah()+"')");
			//Toast.makeText(myContext, "Pesan Terkirim", Toast.LENGTH_SHORT).show();
			Keuangan uang = this.getKeuangan();
			long sisa = uang.getSaldo() - k.getJumlah();
			
			masuk = this.updateKeuangan(""+sisa);
		}
		catch (SQLiteException ex)
		{
			masuk = false;
			Toast.makeText(myContext, ex.getMessage(), Toast.LENGTH_SHORT).show();
		}
		
		
		db.close();
		return masuk;
	}
	
	/**
	 * Untuk mengupdate pengeluaran
	 * @param id
	 * @param nama
	 * @param jumlah
	 * @return
	 */
	public boolean updatePengeluaran(int id, String nama, String jumlah) {
		Pengeluaran p = this.getPengeluaran(id);
		long jumlahAwal = p.getJumlah();
		long jumlahAkhir = Long.parseLong(jumlah);
		long sisa = jumlahAkhir - jumlahAwal;
		boolean masuk = false;
		SQLiteDatabase db = this.getWritableDatabase();
		try {
			
			db.execSQL("update pengeluaran set nama_pengeluaran='"+nama+"', jumlah='"+jumlah+"' where id_pengeluaran='"+id+"'");
			Keuangan uang = this.getKeuangan();
			long sisaAkhir = uang.getSaldo() - (sisa);
			
			masuk = this.updateKeuangan(""+sisaAkhir);
			
		} catch (SQLiteException ex) {
			// TODO: handle exception
			Toast.makeText(myContext, ex.getMessage(), Toast.LENGTH_SHORT).show();
		}
		
		db.close();
		
		return masuk;
	}
	
	/**
	 * Untuk menghapus pengeluaran
	 * @return
	 */
	public boolean deletePengeluaran() {
		ArrayList<Pengeluaran> daftar = this.getAllPengeluaran();
		long jumlah = 0;
		for (Pengeluaran p : daftar)
		{
			jumlah += p.getJumlah();
		}
		
		SQLiteDatabase db = this.getWritableDatabase();
		boolean masuk = false;
		try {
			
			
			
			db.execSQL("delete from pengeluaran");
			
			Keuangan uang = this.getKeuangan();
			long sisa = jumlah + uang.getSaldo();
			
			masuk = this.updateKeuangan(""+sisa);
			
		} catch (SQLiteException ex) {
			// TODO: handle exception
			Toast.makeText(myContext, ex.getMessage(), Toast.LENGTH_SHORT).show();
			masuk = false;
			
		}
		
		db.close();
		return masuk;
	}
	
	/**
	 * untuk menghapus pengeluaran
	 * @param id
	 * @return
	 */
	public boolean deletePengeluaran(int id) {
		Pengeluaran p = this.getPengeluaran(id);
		long jumlah = p.getJumlah();
		SQLiteDatabase db = this.getWritableDatabase();
		boolean masuk = false;
		try {
			
			
			db.execSQL("delete from pengeluaran where id_pengeluaran='"+id+"'");
			
			Keuangan uang = this.getKeuangan();
			long sisa = jumlah + uang.getSaldo();
			
			masuk = this.updateKeuangan(""+sisa);
			
		} catch (SQLiteException ex) {
			// TODO: handle exception
			Toast.makeText(myContext, ex.getMessage(), Toast.LENGTH_SHORT).show();
			masuk = false;
			
		}
		
		db.close();
		return masuk;
	}
	
	//6,5,4,3 
	//1,2,3,4
	/**
	 * Untuk mendapatkan semua data pengeluaran
	 * @return
	 */
	public ArrayList<Pengeluaran> getAllPengeluaran()
	{
		SQLiteDatabase db = this.getReadableDatabase();
		//int i=1;
		ArrayList<Pengeluaran> array = new ArrayList<Pengeluaran>();
		
		Cursor cs = db.rawQuery("select * from "+tabel_pengeluaran+" order by id_pengeluaran desc", null);
		
		while (cs.moveToNext())
		{
			Pengeluaran k = new Pengeluaran();
			k.setId_pengeluaran(cs.getInt(cs.getColumnIndex("id_pengeluaran")));
			k.setNama_pengeluaran(cs.getString(cs.getColumnIndex("nama_pengeluaran")));
			k.setTanggal(cs.getString(cs.getColumnIndex("tanggal")));
			k.setJam(cs.getString(cs.getColumnIndex("jam")));
			k.setJumlah(cs.getLong(cs.getColumnIndex("jumlah")));
			
			array.add(k);
		}
		
		db.close();
		
		return array;
	}
	
	/**
	 * untuk mendapatkan data pengeluaran berdasarkan tanggal
	 * @param date
	 * @return
	 */
	public ArrayList<Pengeluaran> getAllPengeluaran(Date date)
	{
		SQLiteDatabase db = this.getReadableDatabase();
		//int i=1;
		ArrayList<Pengeluaran> array = new ArrayList<Pengeluaran>();
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		
		Cursor cs = db.rawQuery("select * from "+tabel_pengeluaran+" where tanggal='"+format.format(date)+"' order by id_pengeluaran desc", null);
		
		while (cs.moveToNext())
		{
			Pengeluaran k = new Pengeluaran();
			k.setId_pengeluaran(cs.getInt(cs.getColumnIndex("id_pengeluaran")));
			k.setNama_pengeluaran(cs.getString(cs.getColumnIndex("nama_pengeluaran")));
			k.setTanggal(cs.getString(cs.getColumnIndex("tanggal")));
			k.setJam(cs.getString(cs.getColumnIndex("jam")));
			k.setJumlah(cs.getLong(cs.getColumnIndex("jumlah")));
			
			array.add(k);
		}
		
		db.close();
		
		return array;
	}
	
	/**
	 * untuk mendapatkan data pengeluaran berdasarkan bulan
	 * @param bulan
	 * @return
	 */
	public ArrayList<Pengeluaran> getAllPengeluaran(String bulan)
	{
		SQLiteDatabase db = this.getReadableDatabase();
		//int i=1;
		ArrayList<Pengeluaran> array = new ArrayList<Pengeluaran>();
		SimpleDateFormat format = new SimpleDateFormat("yyyy");
		String tahun = format.format(new Date());
		Cursor cs = db.rawQuery("select * from "+tabel_pengeluaran+" where tanggal like '%/"+bulan+"/"+tahun+"' order by id_pengeluaran desc", null);
		
		while (cs.moveToNext())
		{
			Pengeluaran k = new Pengeluaran();
			k.setId_pengeluaran(cs.getInt(cs.getColumnIndex("id_pengeluaran")));
			k.setNama_pengeluaran(cs.getString(cs.getColumnIndex("nama_pengeluaran")));
			k.setTanggal(cs.getString(cs.getColumnIndex("tanggal")));
			k.setJam(cs.getString(cs.getColumnIndex("jam")));
			k.setJumlah(cs.getLong(cs.getColumnIndex("jumlah")));
			
			array.add(k);
		}
		
		db.close();
		
		return array;
	}
	
	/**
	 * untuk mendapatkan pengeluaran berdasarkan id
	 * @param id
	 * @return
	 */
	public Pengeluaran getPengeluaran(int id)
	{
		SQLiteDatabase db = this.getReadableDatabase();
		Pengeluaran k = new Pengeluaran();
		Cursor cs = db.rawQuery("select * from "+tabel_pengeluaran+" where id_pengeluaran='"+id+"'", null);
		if (cs.moveToFirst())
		{
			k.setId_pengeluaran(cs.getInt(cs.getColumnIndex("id_pengeluaran")));
			k.setNama_pengeluaran(cs.getString(cs.getColumnIndex("nama_pengeluaran")));
			k.setTanggal(cs.getString(cs.getColumnIndex("tanggal")));
			k.setJam(cs.getString(cs.getColumnIndex("jam")));
			k.setJumlah(cs.getLong(cs.getColumnIndex("jumlah")));
		}
		
		db.close();
		
		return k;
	}
	
	/**
	 * untuk mendapatkan pengeluaran berdasarkan nama pengeluaran
	 * @param nama
	 * @return
	 */
	
	public ArrayList<Pengeluaran> getPengeluaran(String nama) {
		SQLiteDatabase db = this.getReadableDatabase();
		ArrayList<Pengeluaran> daftar = new ArrayList<Pengeluaran>();
		
		try {
			Cursor cs = db.rawQuery("select * from pengeluaran where nama_pengeluaran like '%"+nama+"%'", null);
			while (cs.moveToNext())
			{
				Pengeluaran p =new Pengeluaran();
				p.setId_pengeluaran(cs.getInt(cs.getColumnIndex("id_pengeluaran")));
				p.setJam(cs.getString(cs.getColumnIndex("jam")));
				p.setJumlah(cs.getLong(cs.getColumnIndex("jumlah")));
				p.setNama_pengeluaran(cs.getString(cs.getColumnIndex("nama_pengeluaran")));
				p.setTanggal(cs.getString(cs.getColumnIndex("tanggal")));
				
				daftar.add(p);
				
			}
		} catch (SQLiteException ex) {
			// TODO: handle exception
			ex.printStackTrace();
		}
		
		db.close();
		
		return daftar;
	}
	
	//TODO pemasukan
	/**
	 * untuk mendapatkan data pemasukan
	 * @return
	 */
	public ArrayList<Pemasukan> getAllPemasukan() {
		SQLiteDatabase db = this.getReadableDatabase();
		ArrayList<Pemasukan> daftar = new ArrayList<Pemasukan>();
		Cursor cs = db.rawQuery("select * from "+tabel_pemasukan, null);
		while (cs.moveToNext())
		{
			Pemasukan k = new Pemasukan();
			k.setId_pemasukan(cs.getInt(cs.getColumnIndex("id_pemasukan")));
			k.setNama_pemasukan(cs.getString(cs.getColumnIndex("nama_pemasukan")));
			k.setTanggal(cs.getString(cs.getColumnIndex("tanggal")));
			k.setJam(cs.getString(cs.getColumnIndex("jam")));
			k.setJumlah(cs.getLong(cs.getColumnIndex("jumlah")));
			
			daftar.add(k);
		}
		
		db.close();
		return daftar;
		
	}
	
	/**
	 * untuk mendapatkan pemasukan berdasarkan id pemasukan
	 * @param id
	 * @return
	 */
	public Pemasukan getPemasukanById(int id) {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cs = db.rawQuery("select * from "+tabel_pemasukan+" where id_pemasukan='"+id+"'", null);
		Pemasukan p = new Pemasukan();
		if (cs.moveToFirst())
		{
			p.setId_pemasukan(cs.getInt(cs.getColumnIndex("id_pemasukan")));
			p.setNama_pemasukan(cs.getString(cs.getColumnIndex("nama_pemasukan")));
			p.setTanggal(cs.getString(cs.getColumnIndex("tanggal")));
			p.setJumlah(cs.getLong(cs.getColumnIndex("jumlah")));
			p.setJam(cs.getString(cs.getColumnIndex("jam")));
		}
		
		db.close();
		return p;
	}
	
	/**
	 * untuk mendapatkan pemasukan berdasarkan bulan
	 * @param bulan
	 * @return
	 */
	public ArrayList<Pemasukan> getPemasukan(String bulan) {
		SQLiteDatabase db = this.getReadableDatabase();
		ArrayList<Pemasukan> daftar = new ArrayList<Pemasukan>();
		SimpleDateFormat format = new SimpleDateFormat("yyyy");
		String tahun = format.format(new Date());
		
		Cursor cs = db.rawQuery("select * from "+tabel_pemasukan+" where tanggal like '%/"+bulan+"/"+tahun+"'", null);
		while (cs.moveToNext())
		{
			Pemasukan k = new Pemasukan();
			k.setId_pemasukan(cs.getInt(cs.getColumnIndex("id_pemasukan")));
			k.setNama_pemasukan(cs.getString(cs.getColumnIndex("nama_pemasukan")));
			k.setTanggal(cs.getString(cs.getColumnIndex("tanggal")));
			k.setJam(cs.getString(cs.getColumnIndex("jam")));
			k.setJumlah(cs.getLong(cs.getColumnIndex("jumlah")));
			
			daftar.add(k);
		}
		
		db.close();
		return daftar;
	}
	
	/**
	 * untuk insert pemasukan
	 * @param pemasukan
	 * @return
	 */
	public boolean insertPemasukan(Pemasukan pemasukan) {
		SQLiteDatabase db = this.getWritableDatabase();
		boolean masuk = false;
		try {
			db.execSQL("insert into pemasukan (nama_pemasukan, tanggal, jam, jumlah) values('"+pemasukan.getNama_pemasukan()+"','"+pemasukan.getTanggal()+"','"+pemasukan.getJam()+"','"+pemasukan.getJumlah()+"')");
			Keuangan uang = this.getKeuangan();
			long sisa = uang.getSaldo() + pemasukan.getJumlah();
			masuk = this.updateKeuangan(""+sisa);
			
		} catch (Exception e) {
			
			e.printStackTrace();
			masuk = false;
		}
		
		db.close();
		return masuk;
		
	}
	
	/**
	 * untuk update pemasukan
	 * @param id
	 * @param nama
	 * @param jumlah
	 * @return
	 */
	public boolean  updatePemasukan(int id, String nama, String jumlah) {
		Pemasukan p = this.getPemasukanById(id);
		long jumlahAwal = p.getJumlah();
		long jumlahAkhir = Long.parseLong(jumlah);
		long sisa = jumlahAkhir - jumlahAwal;
		
		SQLiteDatabase db = this.getWritableDatabase();
		boolean masuk = false;
		
		try {
			db.execSQL("update pemasukan set nama_pemasukan='"+nama+"', jumlah='"+jumlah+"' where id_pemasukan='"+id+"'");
			Keuangan uang = this.getKeuangan();
			long sisakAkhir = uang.getSaldo() + (sisa);
			masuk = this.updateKeuangan(""+sisakAkhir);
			
		} catch (SQLiteException ex) {
			// TODO: handle exception
			Toast.makeText(myContext, ex.getMessage(), Toast.LENGTH_SHORT).show();
		}
		
		db.close();
		
		return masuk;
	}
	

	/**
	 * untuk menghapus pemasukan
	 * @return
	 */
	public boolean deletePemasukan() {


		long jumlah = 0;
		ArrayList<Pemasukan> daftar = this.getAllPemasukan();
		for(Pemasukan p : daftar)
		{
			jumlah += p.getJumlah();
		}
		
		
		SQLiteDatabase db = this.getWritableDatabase();
		boolean masuk = false;
		try {
			
			
			db.execSQL("delete from pemasukan");
			
			Keuangan uang = this.getKeuangan();
			long sisa = uang.getSaldo() - jumlah;
			
			masuk = this.updateKeuangan(""+sisa);
		} catch (SQLiteException ex) {
			// TODO: handle exception
			masuk = false;
			Toast.makeText(myContext, ex.getMessage(), Toast.LENGTH_SHORT).show();
		}
		
		db.close();
		return masuk;
	}
	

	/**
	 * untuk menghapus pemasukan berdasarkan id
	 * @param id
	 * @return
	 */
	public boolean deletePemasukan(int id) {

		Pemasukan p = this.getPemasukanById(id);
		long jumlah = p.getJumlah();
		SQLiteDatabase db = this.getWritableDatabase();
		boolean masuk = false;
		try {
			
			
			
			db.execSQL("delete from pemasukan where id_pemasukan='"+id+"'");
			
			Keuangan uang = this.getKeuangan();
			long sisa = uang.getSaldo() - jumlah;
			
			masuk = this.updateKeuangan(""+sisa);
		} catch (SQLiteException ex) {
			// TODO: handle exception
			masuk = false;
			Toast.makeText(myContext, ex.getMessage(), Toast.LENGTH_SHORT).show();
		}
		
		db.close();
		return masuk;
	}
	
	/**
	 * untuk mengupdate keuangan
	 * @param uang
	 * @return
	 */
	public boolean updateKeuangan(String uang) {
		SQLiteDatabase db = this.getWritableDatabase();
		boolean masuk = false;
		try {
			db.execSQL("update keuangan set saldo='"+uang+"'");
			masuk = true;
		} catch (Exception e) {
			masuk = false;
			e.printStackTrace();
		}
		
		db.close();
		return masuk;
	}
	
	/**
	 * untuk mendapatkan keuangan
	 * @return
	 */
	public Keuangan getKeuangan()
	{
		SQLiteDatabase db = this.getReadableDatabase();
		Keuangan k = new Keuangan();
		Cursor cs = db.rawQuery("select * from "+tabel_keuangan, null);
		if (cs.moveToFirst())
		{
			k.setId_keuangan(cs.getInt(cs.getColumnIndex("id_keuangan")));
			
			k.setSaldo(cs.getLong(cs.getColumnIndex("saldo")));
		}
		
		db.close();
		return k;
	}
	
	

}
